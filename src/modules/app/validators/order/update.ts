import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, Min, MinLength, IsNumber } from 'class-validator';
import { IOrder } from 'modules/database/interfaces/order';

export class UpdateOrderValidator implements Partial<IOrder> {
  @IsOptional()
  @IsInt()
  @Min(0)
  @ApiProperty({ required: false, type: 'integer' })
  public id?: number;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @MinLength(2)
  @MaxLength(500)
  @ApiProperty({ required: true, type: 'string', minLength: 2, maxLength: 500 })
  public description?: string;

  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @ApiProperty({ required: true, type: 'number' })
  public quantity?: number;

  @IsOptional()
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @ApiProperty({ required: true, type: 'number' })
  public value?: number;
}
