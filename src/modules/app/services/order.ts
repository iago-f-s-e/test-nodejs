import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { ICurrentUser } from 'modules/common/interfaces/currentUser';
import { IPaginationParams } from 'modules/common/interfaces/pagination';
import { IOrder } from 'modules/database/interfaces/order';
import { Order } from 'modules/database/models/order';
import { Page } from 'objection';
import { OrderRepository } from '../repositories/order';
import { UserRepository } from '../repositories/user';

@Injectable()
export class OrderService {
  constructor(private readonly orderRepository: OrderRepository, private readonly userRepository: UserRepository) {}
  public async list(params: IPaginationParams, currentUser: ICurrentUser): Promise<Page<Order>> {
    return this.orderRepository.list(params, currentUser.id);
  }

  public async remove(orderId: number, currentUser: ICurrentUser): Promise<void> {
    const order = await this.orderRepository.findById(orderId);

    if (!order) {
      throw new NotFoundException();
    }

    if (order.userId !== currentUser.id) throw new ConflictException('user-unavailable');

    return this.orderRepository.remove(order.id);
  }

  public async findById(orderId: number, currentUser: ICurrentUser): Promise<Order> {
    const order = await this.orderRepository.findById(orderId);

    if (!order) {
      throw new NotFoundException();
    }

    if (order.userId !== currentUser.id) throw new ConflictException('user-unavailable');

    return order;
  }

  public async save(model: IOrder): Promise<Order> {
    const user = await this.userRepository.findById(model.userId);

    if (!user) throw new NotFoundException();

    return this.orderRepository.insert({ ...model, user });
  }

  public async update(modal: Partial<IOrder>, orderId: number, currentUser: ICurrentUser): Promise<Order> {
    // eslint-disable-next-line
    const { id, user, userId, ...updateModal } = modal;

    const order = await this.orderRepository.findById(orderId);

    if (!order) {
      throw new NotFoundException();
    }

    if (order.userId !== currentUser.id) throw new ConflictException('user-unavailable');

    return this.orderRepository.update({
      ...order,
      ...updateModal
    });
  }
}
