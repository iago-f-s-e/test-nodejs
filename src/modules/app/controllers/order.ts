import { Body, Controller, Post, Put, Param, ParseIntPipe, Get, Delete, Query } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthRequired, CurrentUser } from 'modules/common/guards/token';
import { ICurrentUser } from 'modules/common/interfaces/currentUser';
import { Order } from 'modules/database/models/order';
import { OrderService } from '../services/order';
import { SaveOrderValidator, UpdateOrderValidator, ListOrderValidator } from '../validators/order';

@ApiTags('App: Order')
@Controller('/order')
@AuthRequired()
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @Delete(':orderId')
  @ApiResponse({ status: 200 })
  public async delete(@Param('orderId', ParseIntPipe) orderId: number, @CurrentUser() currentToken: ICurrentUser) {
    return this.orderService.remove(orderId, currentToken);
  }

  @Get(':orderId')
  @ApiResponse({ status: 200, type: Order })
  public async details(@Param('orderId', ParseIntPipe) orderId: number, @CurrentUser() currentToken: ICurrentUser) {
    return this.orderService.findById(orderId, currentToken);
  }

  @Put(':orderId')
  @ApiResponse({ status: 200, type: Order })
  public async update(
    @Param('orderId', ParseIntPipe) orderId: number,
    @Body() model: UpdateOrderValidator,
    @CurrentUser() currentToken: ICurrentUser
  ) {
    return this.orderService.update(model, orderId, currentToken);
  }

  @Get()
  @ApiResponse({ status: 200, type: [Order] })
  public async list(@Query() query: ListOrderValidator, @CurrentUser() currentToken: ICurrentUser) {
    return this.orderService.list(query, currentToken);
  }

  @Post()
  @ApiResponse({ status: 201, type: Order })
  public async save(@Body() model: SaveOrderValidator, @CurrentUser() currentToken: ICurrentUser) {
    return this.orderService.save({
      ...model,
      userId: currentToken.id
    });
  }
}
